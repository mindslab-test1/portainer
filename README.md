# 2021-10-19 IBK 음성봇 설치 관련

# STT / TTS 설치 과정

STT / TTS는 Docker로 운영.

# 필요 파일 가져오기

## STT / TTS 학습 데이터 가져오기

```
#STT 파일
scp w2l_20200318_reDATA.tar root@10.122.64.133:/DATA  #학습 데이터 파일이 있는 서버에서 진행 시
sudo scp minds@10.122.64.42:/DATA/w2l_20200318_reDATA.tar . # 본서버의 /DATA/에서 진행 시

#TTS 파일
scp tts_DATA.tar root@10.122.64.133:/DATA  #학습 데이터 파일이 있는 서버에서 진행 시
sudo scp minds@10.122.64.42:/DATA/tts_DATA.tar . # 본서버의 /DATA/에서 진행 시

tar -xvf w2l_20200318_reDATA.tar
tar -xvf tts_DATA.tar
```
## 가져온 STT 데이터의 server.cfg 수정
-   '/DATA/w2l_20200318/base_kor_8k_eos/'로 되어 있는 경로를 '/MODEL/' 경로로 수정  
```
cd /DATA/w2l_20200318/base_kor_8k_eos

vi server.cfg

# Decoding config for Librispeech using Gated ConvNets
# Replace `[...]` with appropriate paths
# for test-other (best params for dev-other)
--tokensdir=/MODEL/am
--lexicon=
--lm=/MODEL/lm_tkn_eos/lm.arpa.bin
--am=/MODEL/model/005_model_dev-lg.bin
--decodertype=tkn
--uselexicon=false
--lmweight=2.9728460167409256
--wordscore=2.9344071407238204
--silweight=-0.4596029083087377
--beamsize=500
--beamthreshold=10
--nthread_decoder=300
--smearing=max
--usemecab=true
--usenormalizer=true
--usedetailsegment=true
--samplerate=8000
--v=1
--vadremote=127.0.0.1:14001
```

## Docker-compose 파일 모음

```
cd /home/minds/git
git clone https://pms.maum.ai/bitbucket/scm/aiccs/portainer.git
cd portainer
```

-   w2l : STT 관련
-   tts : TTS 관련

# 1. STT 설치 과정
/home/minds/git/portainer 기준
```
cd w2l
```

## docker-compose.yml 명령어 추가

-   vad: 와 server:에 'restart : always' 추가 (container를 재실행할때 같이 재실행 된다.)

-   Log 확인을 위해 server: command 끝에 '--v=1' 추가

```python
vim docker-compose.yml

...
vad:
    ...
    restart : always
    ...

server:
    ...
    command: --nthread_decoder=10 --vadremote=172.17.0.1:14001 --usedetailsegment=false --v=1
    restart : always
    ...
```

## docker container 생성 및 실행

```
docker-compose -f  docker-compse.yml up -d
```

## Log를 통해 정상적으로 생성되었는지 확인

```
docker logs w2l_server_1
docker logs w2l_vad_1
```

# 2. TTS 설치 과정

portainer 기준

```
cd tts
```

## docker-compose-engine.yml / docker-compose-pre-wo-rest.yml에 명령어 추가

-   각 services: 아래에 'restart : always' 추가 (container를 재실행할때 같이 재실행 된다.)

```
restart : always
```

## docker-compose-pre-wo-rest.yml port 수정

현재 9999 포트는 m2u-rest에서 점유 중이기에 9997번 포트로 변경

```
services:
    grpc:
        ports: 9997:9999 #9999:9999에서 변경하기

    ....

    custom:
        environment:
            ...
            - "GRPC_ADDR_TTS_PORT=9997" # 9999에서 변경
            ...
```

## docker container 생성 및 실행

```
docker-compose -f docker-compose-engine.yml -f docker-compose-pre-wo-rest.yml up -d
```

## container 정상적으로 생성되었는지 확인(tts\_ 관련 7가지)

```
docker ps -a


CONTAINER ID   IMAGE                                           COMMAND                  CREATED       STATUS       PORTS                                                                                                                                            NAMES
af8ae915e8d9   docker.maum.ai:443/brain/tts:1.2.12-server      "java -jar -Dlog4j.c…"   4 hours ago   Up 4 hours   0.0.0.0:9997->9999/tcp, :::9997->9999/tcp                                                                                                        tts_grpc_1
de4ca90a3c2a   mcr.microsoft.com/mssql/server:2019-latest      "/opt/mssql/bin/perm…"   4 hours ago   Up 4 hours   0.0.0.0:1433->1433/tcp, :::1433->1433/tcp                                                                                                        mssql-server
26ffc23aa51c   portainer/portainer-ce:alpine                   "/portainer"             4 hours ago   Up 4 hours   8000/tcp, 9443/tcp, 0.0.0.0:8880->9000/tcp, :::8880->9000/tcp                                                                                    portainer
b030cb92ef5c   docker.maum.ai:443/tts-custom:1.0.3             "/sbin/init"             6 hours ago   Up 6 hours   0.0.0.0:9090->9090/tcp, :::9090->9090/tcp, 0.0.0.0:9998->9998/tcp, :::9998->9998/tcp, 0.0.0.0:19999->19999/tcp, :::19999->19999/tcp, 30999/tcp   tts_custom_1
11104eab39ce   docker.maum.ai:443/brain/hifigan:0.3.2-server   "/bin/sh -c 'python …"   6 hours ago   Up 6 hours   0.0.0.0:35101->35003/tcp, :::35101->35003/tcp                                                                                                    tts_hifigan_1
aab4c7fe6738   docker.maum.ai:443/brain/konglish:1.0-server    "python server.py --…"   6 hours ago   Up 6 hours   0.0.0.0:20001->20001/tcp, :::20001->20001/tcp                                                                                                    tts_konglish_1
82e88a976f9b   docker.maum.ai:443/rbkog2p:0.5.0                "/bin/sh -c 'python …"   6 hours ago   Up 6 hours   0.0.0.0:19002->19002/tcp, :::19002->19002/tcp                                                                                                    tts_g2p_kor_1
02ca3db21e2b   docker.maum.ai:443/brain_dca:1.1.2-server       "/bin/sh -c 'python …"   6 hours ago   Up 6 hours   0.0.0.0:30101->30001/tcp, :::30101->30001/tcp                                                                                                    tts_dca_1
6ba0d9751050   docker.maum.ai:443/sdn3:0.0.5                   "python3 /root/sdn3/…"   6 hours ago   Up 6 hours   50051/tcp, 0.0.0.0:50051->31050/tcp, :::50051->31050/tcp                                                                                         tts_sdn_1
898aa3329f59   docker.maum.ai:443/brain/vad:0.1.1-server       "/bin/sh -c 'python3…"   6 hours ago   Up 6 hours   0.0.0.0:14001->14001/tcp, :::14001->14001/tcp                                                                                                    w2l_vad_1
a33810b852f2   docker.maum.ai:443/brain/w2l:1.1.7-server       "./Server --flagsfil…"   6 hours ago   Up 6 hours   0.0.0.0:15001->15001/tcp, :::15001->15001/tcp                                                                                                    w2l_server_1
f9de1e56852b   mysql:latest                                    "docker-entrypoint.s…"   6 hours ago   Up 6 hours   0.0.0.0:3306->3306/tcp, :::3306->3306/tcp, 33060/tcp                                                                                             mysql-server
8462c50efc87   docker.maum.ai:443/qa/nlp3:1.0.0v               "/bin/bash -c ./root…"   5 days ago    Up 5 days                                                                                                                                                     nlp3-server

```

## tts_custom container 추가 설정

### tts-admin ip 설정

```
docker exec -it tts_custom_1 bash

cd bin/tts-admin

vim application.yml
```

-   ip를 맞게 변경해주기

```
tts:
  maxSpeakerId: 2
  ip: http://10.122.64.133:9998 #http://{Host IP} 형태로 지정해 주어야 함
```

### 이후 custom의 README.md의 Docker container 생성 및 실행 부분 이어서 진행

```
[mindslab@localhost tts]$ docker exec -it tts_custom_1 bash
[root@eba800619203 bin]# ./tts-start
success start
[root@eba800619203 bin]# ./rest-start
profile dev
success start
[root@eba800619203 bin]# cd tts-admin/
[root@eba800619203 tts-admin]# ./start.sh
TTS Admin start...
[root@eba800619203 tts-admin]# ps -ef
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 15:29 ?        00:00:00 /sbin/init
root        20     1  0 15:29 ?        00:00:00 /usr/lib/systemd/systemd-journald
root        30     1  0 15:29 ?        00:00:00 /usr/lib/systemd/systemd-udevd
root        85     1  0 15:29 ?        00:00:00 /usr/lib/systemd/systemd-logind
dbus        86     1  0 15:29 ?        00:00:00 /usr/bin/dbus-daemon --system --address=systemd: --nofork --nopi
root        90     1  0 15:29 tty1     00:00:00 /sbin/agetty --noclear tty1 linux
mysql      100     1  0 15:29 ?        00:00:00 /usr/sbin/mysqld
root       134     0  0 15:29 pts/0    00:00:00 bash
root       166     0 99 15:30 pts/0    00:00:14 java -jar -DNODE_NAME=custom_server_dev_eba800619203 -Dlog4j.con
root       217     0 99 15:30 pts/0    00:00:34 /usr/bin/java -jar -DNODE_NAME=tts_rest_dev_eba800619203 -Dsite=
root       268     0 99 15:30 pts/0    00:00:18 java -jar /srv/maum/bin/tts-admin/tts-package.war --spring.confi
root       304   134  0 15:31 pts/0    00:00:00 ps -ef
```

## tts-admin 실행 확인

-   아래 주소로 접속하여 정상적으로 실행되는지 확인

```
server_ip:9090
```

# 3. 추가 설정

## Bitbucket에서 MAUM-CONNECT Projects에 있는 brain-stt 가져오기

```
git clone https://pms.maum.ai/bitbucket/scm/maumconnect/brain-stt.git
```

이후 brain-stt/src/stt-proxy/README.md에 있는 내용 수행

