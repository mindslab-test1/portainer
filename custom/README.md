# TTS custom docker image
TTS custom 도커 이미지에는 
+ TTS custom
+ TTS rest
+ TTS admin
+ TTS admin에 필요한 mariadb
가 같이 설치되어 있음

*Notice* 
- brain팀에서 만든 tts-rest 이미지를 사용하여 컨테이너를 실행하면 tts admin과 연동되지 않음
- 여기 포함된 TTS rest는 브레인팀의 tts-rest 이미지와 다른 버전임. TTS admin과 연동하려면 여기 포함된 TTS rest를 사용하고 브레인팀의 tts-rest는 동작시키지 않아야 함
- `yum -y update` 하지 말것: 업데이트 중에 ko_KR 등 locale 설치가 삭제됨
 
## Image tag 1.0.1
---
custom image 1.0.0기반으로 이미지를 새로 작성
- /srv/maum/resource 경로명 오류: /srv/maum/resources로 변경
### MariaDB 설치
```
vi /etc/yum.repos.d/MariaDB.repo
```
vim 편집기로 위의 파일을 열어서 아래 내용을 복사하고 저장
```
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.4/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
```
MariaDB 설치
```
yum install MariaDB
```
### ffmpeg, sox  설치
*중요* ffmpeg, sox 설치되어야 tts-rest에서 변환 가능(TTS 어드민 화면에서 변환 기능 사용)
- ffmpeg, sox 설치
```bash
yum localinstall --nogpgcheck https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm
yum install ffmpeg ffmpeg-devel
yum install sox
```
### 엔진 실행 파일 복사
+ tts-rest 파일 복사
+ tts-admin 파일 복사
+ tts-custom jar 파일 업데이트
__여기까지 작업 후 1.0.1로 commit__

---

## Image tag 1.0.2
---
+ MariaDB를 시스템 서비스에 등록하여 자동으로 기동되도록 설정
```console
[root@170ed2789223 maum]# systemctl start mariadb
[root@170ed2789223 maum]# systemctl enable mariadb
Created symlink from /etc/systemd/system/mysql.service to /usr/lib/systemd/system/mariadb.service.
Created symlink from /etc/systemd/system/mysqld.service to /usr/lib/systemd/system/mariadb.service.
Created symlink from /etc/systemd/system/multi-user.target.wants/mariadb.service to /usr/lib/systemd/system/mariadb.service.
```

+ tts.sql을 mariadb에 import 
```bash
cd /srv/maum/bin/tts-admin
mysql -uroot -pMindslab1!
grant all privileges on *.* to 'root'@'%' identified by 'Mindslab1!';
flush privileges;
create database TTS;
mysql -uroot -pMindslab1! TTS < tts.sql
```

__여기까지 작업 후 1.0.2로 commit__

## Image tag 1.0.3
---
+ tts-rest 설정 파일 복사
+ tts-custom 설정 파일 복사
+ 로그 설정 수정
+ `/srv/maum/resources`의 파일들 tar로 묶어 복사
```
scp fast@221.168.33.192:/srv/maum/resources/resources_tts.tar .
```
+ tts-start shell에 환경 변수 추가
+ tts admin 설정에서 tts rest를 바라볼 때 반드시 아래와 같이 설정해야 동작함
```yaml
tts:
  maxSpeakerId: 2
  ip: http://10.122.64.95:9998 #http://{Host IP} 형태로 지정해 주어야 함
```
__여기까지 작업 후 1.0.3으로 commit__

## Docker container 생성 및 실행
+ 1.0.3 이미지로 docker-compose 실행하고 정상동작 확인
```console
[mindslab@localhost tts]$ docker-compose -f docker-compose-pre-wo-rest.yml up -d
Recreating tts_custom_1 ... done
Creating tts_g2p_kor_1  ... done
Creating tts_sdn_1      ... done
Creating tts_grpc_1     ... done
Creating tts_konglish_1 ... done


[mindslab@localhost tts]$ docker exec -it tts_custom_1 bash
[root@eba800619203 bin]# ./tts-start 
success start
[root@eba800619203 bin]# ./rest-start 
profile dev
success start
[root@eba800619203 bin]# cd tts-admin/
[root@eba800619203 tts-admin]# ./start.sh
TTS Admin start...
[root@eba800619203 tts-admin]# ps -ef
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 15:29 ?        00:00:00 /sbin/init
root        20     1  0 15:29 ?        00:00:00 /usr/lib/systemd/systemd-journald
root        30     1  0 15:29 ?        00:00:00 /usr/lib/systemd/systemd-udevd
root        85     1  0 15:29 ?        00:00:00 /usr/lib/systemd/systemd-logind
dbus        86     1  0 15:29 ?        00:00:00 /usr/bin/dbus-daemon --system --address=systemd: --nofork --nopi
root        90     1  0 15:29 tty1     00:00:00 /sbin/agetty --noclear tty1 linux
mysql      100     1  0 15:29 ?        00:00:00 /usr/sbin/mysqld
root       134     0  0 15:29 pts/0    00:00:00 bash
root       166     0 99 15:30 pts/0    00:00:14 java -jar -DNODE_NAME=custom_server_dev_eba800619203 -Dlog4j.con
root       217     0 99 15:30 pts/0    00:00:34 /usr/bin/java -jar -DNODE_NAME=tts_rest_dev_eba800619203 -Dsite=
root       268     0 99 15:30 pts/0    00:00:18 java -jar /srv/maum/bin/tts-admin/tts-package.war --spring.confi
root       304   134  0 15:31 pts/0    00:00:00 ps -ef
```